# InfoDesignSwapi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## O pracy nad projektem

Branche - z racji tego, że sam projekt był prosty, postanowiłam nie pracować na kilku branchach. Jednak z reguły przy bardziej skomplikowanych rzeczach pracuje na 
oddzialenych branchach, każdy do innego zadania. 

Komunikacja między komponentami - podczas pracy z tym projektem chciałam postawić na komnunikację między komponentami dzięki subjectom w osobnym serwisie nazwanym subjects.service.ts (nie ma go
obecnie w projekcie) po to, by utowrzyć osobny komponent do wyświetlania tabelki wybików - jeden komponent do tabeli zamiast dwie tabele, każda w innym komponencie tak jak to wygląda teraz, 
powoduje to zbyt wiele kodu w projekcie i zamieszanie oraz na komunikację input - output między komponentami po to, by po kliknięciu w jeden row tabelki przenieść się w osobny komponent wyświetlający nam 
pojedynczy wynik szczegółowo. za pomocą inputa response z zapytania miał być przekazywania do childa, a child miał rodzicowi kompunikować się, że zamykamy komponent, 
aby ponownie wyświetlić tabelę z wynikami.

Jednak kompunikacja się nie udała. Nastąpił pewien błąd, którego nie umiałam zidentyfikowac i naprawić w bardzo krótkim czasie i podejrzewam, że przyczyna była głębsza niż na poziomie komponentów.
Nie działała w ogóle komunikacja pomiędzy komponentami - żadna, w całym projekcie. Próbowałam również forkJoin, badałam czyt problem leżał w źle napisanych subjectach, czy trzeba było zwracać je .asObservable,
próbowałam różnych sposobów, nie działało nic. Na tego typu problem potrzebowałabym poświęcić zdecydowanie więcej czasu, dlatego kod wygląda jak wygląda.
 Tabela w browse - również wiem, że można było to zrobić inaczej.
