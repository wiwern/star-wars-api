import { Component, OnInit } from '@angular/core';
import { SubjectsService } from './services/subjects.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Star Wars Api';

  public option: string = 'home';


  chooseOption(opt: string) {
    this.option = opt;
  }
}
