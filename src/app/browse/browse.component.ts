import { Component, Input } from '@angular/core';
import { SwapiService } from '../service/swapi.service';
import { Film } from '../types/Film';
import { Person } from '../types/Person';
import { Planet } from '../types/Planet';
import { Specie } from '../types/Specie';
import { Starship } from '../types/Starship';
import { Vehicle } from '../types/Vehicle';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.scss']
})
export class BrowseComponent  {
  @Input('simple') simple: any;
  @Input('type') type: string = '';

  public films:Array<Film> = [];
  public people: Array<Person> = [];
  public starships:Array<Starship> = [];
  public planets:Array<Planet> = [];
  public species:Array<Specie> = [];
  public vehicles:Array<Vehicle> = [];

  public startCardIsShown:boolean = true;

  public filmsAreShown:boolean = false;
  public peopleAreShown:boolean = false;
  public vehiclesAreShown:boolean = false;
  public planetsAreShown:boolean = false;
  public speciesAreShown:boolean = false;
  public starshipsAreShown:boolean = false;
  public activeCard: string = 'films';

  public simpleResult: boolean = false;


  constructor(private swapiService: SwapiService) {

  }


  public showRootInfo(attribute: string) {
    this.swapiService.getBasicResourceFromRoot(attribute).subscribe((response: any) => {
      switch(attribute) {
        case 'films':
          this.films = response.results;
          this.filmsAreShown = true;
          this.activeCard = 'films';
          break;
        case 'people':
          this.people = response.results;
          this.peopleAreShown = true;
          this.activeCard = 'people';
          break;
        case 'starships':
          this.starships = response.results;
          console.log(this.starships);
          this.starshipsAreShown = true;
          this.activeCard = 'starships';
          break;
        case 'vehicles':
          this.vehicles = response.results;
          this.vehiclesAreShown = true;
          this.activeCard = 'vehicles';
          break;
        case 'species':
          this.species = response.results;
          this.speciesAreShown = true;
          this.activeCard = 'species';
          break;
        case 'planets':
          this.planets = response.results;
          this.planetsAreShown = true;
          this.activeCard = 'planets';
          break;
        default:
          break;      
      }
    });


    this.startCardIsShown = false;
  }
  
  public goToBrowse() {
    this.startCardIsShown = true;
    this.filmsAreShown  = false;
    this.peopleAreShown = false;
    this.vehiclesAreShown = false;
    this.planetsAreShown = false;
    this.speciesAreShown = false;
    this.starshipsAreShown = false;

    this.films = [];
    this.people = [];
    this.starships = [];
    this.planets = [];
    this.species = [];
    this.vehicles = [];
  }

  public goToDetails(number: number) {
   
    const id = number + 1,
    type = this.getActualType(),
    objectId = JSON.stringify(id)

    this.swapiService.searchObject(type, objectId).subscribe(response => {
      this.simple = response;
      this.type = type;
    });
  }

  private getActualType(): string {
    let type = '';
    if (this.speciesAreShown) {
      type = 'species';
    }
    if (this.peopleAreShown){
      type = 'people';
    }
    if (this.planetsAreShown) {
      type = 'planets'
    }
    if (this.vehiclesAreShown){
      type = 'vehicles';
    }
    if (this.starshipsAreShown) {
      type = 'staships';
    }
    if(this.filmsAreShown) {
      type = 'films';
    }

    return type;
  }

}
