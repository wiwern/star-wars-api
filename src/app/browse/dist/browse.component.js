"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.BrowseComponent = void 0;
var core_1 = require("@angular/core");
var BrowseComponent = /** @class */ (function () {
    function BrowseComponent(swapiService) {
        this.swapiService = swapiService;
        this.type = '';
        this.films = [];
        this.people = [];
        this.starships = [];
        this.planets = [];
        this.species = [];
        this.vehicles = [];
        this.startCardIsShown = true;
        this.filmsAreShown = false;
        this.peopleAreShown = false;
        this.vehiclesAreShown = false;
        this.planetsAreShown = false;
        this.speciesAreShown = false;
        this.starshipsAreShown = false;
        this.activeCard = 'films';
        this.simpleResult = false;
    }
    BrowseComponent.prototype.showRootInfo = function (attribute) {
        var _this = this;
        this.swapiService.getBasicResourceFromRoot(attribute).subscribe(function (response) {
            switch (attribute) {
                case 'films':
                    _this.films = response.results;
                    _this.filmsAreShown = true;
                    _this.activeCard = 'films';
                    break;
                case 'people':
                    _this.people = response.results;
                    _this.peopleAreShown = true;
                    _this.activeCard = 'people';
                    break;
                case 'starships':
                    _this.starships = response.results;
                    console.log(_this.starships);
                    _this.starshipsAreShown = true;
                    _this.activeCard = 'starships';
                    break;
                case 'vehicles':
                    _this.vehicles = response.results;
                    _this.vehiclesAreShown = true;
                    _this.activeCard = 'vehicles';
                    break;
                case 'species':
                    _this.species = response.results;
                    _this.speciesAreShown = true;
                    _this.activeCard = 'species';
                    break;
                case 'planets':
                    _this.planets = response.results;
                    _this.planetsAreShown = true;
                    _this.activeCard = 'planets';
                    break;
                default:
                    break;
            }
        });
        this.startCardIsShown = false;
    };
    BrowseComponent.prototype.goToBrowse = function () {
        this.startCardIsShown = true;
        this.filmsAreShown = false;
        this.peopleAreShown = false;
        this.vehiclesAreShown = false;
        this.planetsAreShown = false;
        this.speciesAreShown = false;
        this.starshipsAreShown = false;
        this.films = [];
        this.people = [];
        this.starships = [];
        this.planets = [];
        this.species = [];
        this.vehicles = [];
    };
    BrowseComponent.prototype.goToDetails = function (number) {
        var _this = this;
        var id = number + 1, type = this.getActualType(), objectId = JSON.stringify(id);
        this.swapiService.searchObject(type, objectId).subscribe(function (response) {
            _this.simple = response;
            _this.type = type;
        });
    };
    BrowseComponent.prototype.getActualType = function () {
        var type = '';
        if (this.speciesAreShown) {
            type = 'species';
        }
        if (this.peopleAreShown) {
            type = 'people';
        }
        if (this.planetsAreShown) {
            type = 'planets';
        }
        if (this.vehiclesAreShown) {
            type = 'vehicles';
        }
        if (this.starshipsAreShown) {
            type = 'staships';
        }
        if (this.filmsAreShown) {
            type = 'films';
        }
        return type;
    };
    __decorate([
        core_1.Input('simple')
    ], BrowseComponent.prototype, "simple");
    __decorate([
        core_1.Input('type')
    ], BrowseComponent.prototype, "type");
    BrowseComponent = __decorate([
        core_1.Component({
            selector: 'app-browse',
            templateUrl: './browse.component.html',
            styleUrls: ['./browse.component.scss']
        })
    ], BrowseComponent);
    return BrowseComponent;
}());
exports.BrowseComponent = BrowseComponent;
