"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.ResultComponent = void 0;
var core_1 = require("@angular/core");
var ResultComponent = /** @class */ (function () {
    function ResultComponent() {
        this.type = '';
        this.simple = {
            title: '',
            episode_id: 0,
            opening_crawl: '',
            characters: [],
            director: '',
            created: '',
            planets: [],
            producer: '',
            species: [],
            starships: [],
            url: '',
            vehicles: [],
            release_date: ''
        };
    }
    ResultComponent.prototype.ngOnInit = function () {
        // tutaj miała przebiegać komunikacja między parentem, a childem, aby wyświetlić szczegóły klikniętego 
        // elementu w osobnym komponencie. szczegóły opisałam w readme.MD
    };
    ResultComponent = __decorate([
        core_1.Component({
            selector: 'app-result',
            templateUrl: './result.component.html',
            styleUrls: ['./result.component.scss']
        })
    ], ResultComponent);
    return ResultComponent;
}());
exports.ResultComponent = ResultComponent;
