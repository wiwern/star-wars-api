import { Component, OnInit } from '@angular/core';
import { Film } from '../types/Film';
import { Vehicle } from '../types/Vehicle';
import { Starship } from '../types/Starship';
import { Planet } from '../types/Planet';
import { Person } from '../types/Person';
import { Specie } from '../types/Specie';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  simple: any;
  type: string = '';
  constructor() {
    this.simple = {    
      title: '',
      episode_id: 0,
      opening_crawl: '',
      characters: [],
      director: '',
      created: '',
      planets: [],
      producer: '',
      species: [],
      starships: [],
      url:'',
      vehicles: [],
      release_date: ''
    }
   }

  

  ngOnInit(): void {
    // tutaj miała przebiegać komunikacja między parentem, a childem, aby wyświetlić szczegóły klikniętego 
    // elementu w osobnym komponencie. szczegóły opisałam w readme.MD
  }

}
