"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.SearchComponent = void 0;
var core_1 = require("@angular/core");
var SearchComponent = /** @class */ (function () {
    function SearchComponent(swapiService) {
        this.swapiService = swapiService;
        this.type = '';
        this.alert = { type: 'danger',
            message: 'choose a searching data type!' };
        this.activeCard = '';
        this.searchingCategory = '';
        this.simpleResult = false;
        this.search = '';
        this.showAlert = false;
        this.results = [];
        this.films = [];
        this.people = [];
        this.starships = [];
        this.planets = [];
        this.species = [];
        this.vehicles = [];
        this.filmsAreShown = false;
        this.peopleAreShown = false;
        this.vehiclesAreShown = false;
        this.planetsAreShown = false;
        this.speciesAreShown = false;
        this.starshipsAreShown = false;
        this.nothingIsShown = false;
    }
    SearchComponent.prototype.addCategoryToSearching = function (category) {
        this.searchingCategory = category;
    };
    SearchComponent.prototype.searchString = function () {
        var _this = this;
        if (this.searchingCategory === '') {
            this.showAlert = true;
        }
        else {
            this.swapiService.searchPhrase(this.searchingCategory, this.search).subscribe(function (response) {
                _this.results = response.results;
                if (_this.results.length === 0) {
                    _this.nothingIsShown = true;
                }
                else {
                    switch (_this.searchingCategory) {
                        case 'films':
                            _this.films = response.results;
                            _this.filmsAreShown = true;
                            _this.activeCard = 'films';
                            break;
                        case 'people':
                            _this.people = response.results;
                            _this.peopleAreShown = true;
                            _this.activeCard = 'people';
                            break;
                        case 'starships':
                            _this.starships = response.results;
                            _this.starshipsAreShown = true;
                            _this.activeCard = 'starships';
                            break;
                        case 'vehicles':
                            _this.vehicles = response.results;
                            _this.vehiclesAreShown = true;
                            _this.activeCard = 'vehicles';
                            break;
                        case 'species':
                            _this.species = response.results;
                            _this.speciesAreShown = true;
                            _this.activeCard = 'species';
                            break;
                        case 'planets':
                            _this.planets = response.results;
                            _this.planetsAreShown = true;
                            _this.activeCard = 'planets';
                            break;
                        default:
                            _this.nothingIsShown = true;
                    }
                }
            });
        }
    };
    SearchComponent.prototype.goToDetails = function (number) {
        var _this = this;
        var id = number + 1, type = this.getActualType(), objectId = JSON.stringify(id);
        this.swapiService.searchObject(type, objectId).subscribe(function (response) {
            _this.simple = response;
            _this.type = type;
        });
    };
    SearchComponent.prototype.getActualType = function () {
        var type = '';
        if (this.speciesAreShown) {
            type = 'species';
        }
        if (this.peopleAreShown) {
            type = 'people';
        }
        if (this.planetsAreShown) {
            type = 'planets';
        }
        if (this.vehiclesAreShown) {
            type = 'vehicles';
        }
        if (this.starshipsAreShown) {
            type = 'staships';
        }
        if (this.filmsAreShown) {
            type = 'films';
        }
        return type;
    };
    __decorate([
        core_1.Input('simple')
    ], SearchComponent.prototype, "simple");
    __decorate([
        core_1.Input('type')
    ], SearchComponent.prototype, "type");
    SearchComponent = __decorate([
        core_1.Component({
            selector: 'app-search',
            templateUrl: './search.component.html',
            styleUrls: ['./search.component.scss']
        })
    ], SearchComponent);
    return SearchComponent;
}());
exports.SearchComponent = SearchComponent;
