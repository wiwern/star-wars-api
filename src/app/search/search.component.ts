import { Component, OnInit, Input } from '@angular/core';
import { SwapiService } from '../service/swapi.service';
import { Film } from '../types/Film';
import { Person } from '../types/Person';
import { Planet } from '../types/Planet';
import { Specie } from '../types/Specie';
import { Vehicle } from '../types/Vehicle';
import { Starship } from '../types/Starship';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})

export class SearchComponent  {
  @Input('simple') simple: any;
  @Input('type') type: string = '';
  public alert = {type: 'danger', 
                        message: 'choose a searching data type!'}
  

  public activeCard: string = '';
  private searchingCategory: string = '';
  public simpleResult: boolean = false;

  public search: string = '';
  public showAlert = false;
  public results: Array<Film | Person | Planet | Vehicle | Specie | Starship> = [];
  
  public films:Array<Film> = [];
  public people: Array<Person> = [];
  public starships:Array<any> = [];
  public planets:Array<Planet> = [];
  public species:Array<Specie> = [];
  public vehicles:Array<Vehicle> = [];

  public filmsAreShown:boolean = false;
  public peopleAreShown:boolean = false;
  public vehiclesAreShown:boolean = false;
  public planetsAreShown:boolean = false;
  public speciesAreShown:boolean = false;
  public starshipsAreShown:boolean = false;
  public nothingIsShown:boolean = false;

  constructor(private swapiService: SwapiService) { }


  public addCategoryToSearching(category: string): void {
    this.searchingCategory = category;
  }

  public searchString() {
    if (this.searchingCategory === '') {
      this.showAlert = true;
    } else {
      this.swapiService.searchPhrase(this.searchingCategory, this.search).subscribe((response: any) => {
        this.results = response.results;
        if (this.results.length === 0) {
          this.nothingIsShown = true;
        } else {
          switch(this.searchingCategory) {
            case 'films':
              this.films = response.results;
              this.filmsAreShown = true;
              this.activeCard = 'films';
              break;
            case 'people':
              this.people = response.results;
              this.peopleAreShown = true;
              this.activeCard = 'people';
              break;
            case 'starships':
              this.starships = response.results;
              this.starshipsAreShown = true;
              this.activeCard = 'starships';
              break;
            case 'vehicles':
              this.vehicles = response.results;
              this.vehiclesAreShown = true;
              this.activeCard = 'vehicles';
              break;
            case 'species':
              this.species = response.results;
              this.speciesAreShown = true;
              this.activeCard = 'species';
              break;
            case 'planets':
              this.planets = response.results;
              this.planetsAreShown = true;
              this.activeCard = 'planets';
              break; 
            default:
              this.nothingIsShown = true;  
          }
        }
      });
    }
  }

  public goToDetails(number: number) {
    const id = number + 1,
    type = this.getActualType(),
    objectId = JSON.stringify(id)

    this.swapiService.searchObject(type, objectId).subscribe(response => {
      this.simple = response;
      this.type = type;
    });

  }

  private getActualType(): string {
    let type = '';
    if (this.speciesAreShown) {
      type = 'species';
    }
    if (this.peopleAreShown){
      type = 'people';
    }
    if (this.planetsAreShown) {
      type = 'planets'
    }
    if (this.vehiclesAreShown){
      type = 'vehicles';
    }
    if (this.starshipsAreShown) {
      type = 'staships';
    }
    if(this.filmsAreShown) {
      type = 'films';
    }

    return type;
  }
  
}


