import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  swapiArraySubject: Subject<Array<any>>;
  
  swapiArrayTypeSubject: Subject<string>;

  constructor() {
    this.swapiArraySubject = new Subject<Array<any>>();

    this.swapiArrayTypeSubject = new Subject<string>();
    
   }

  public transferArray(array: Array<any>): void {
    console.log(array)
    this.swapiArraySubject.next(array);
  }

   transferType(type:string) {
     console.log(type);
     this.swapiArrayTypeSubject.next(type);
   }

   getType(): Observable<string> {
     return this.swapiArrayTypeSubject.asObservable();
   }

   

}
