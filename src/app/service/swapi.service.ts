import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SwapiService {

  url: string = 'https://swapi.dev/api/';

  constructor(private http: HttpClient) { }

  getBasicResourceFromRoot(resource:string) {
    const url = this.url + resource +'/';
    
    return this.http.get(url);
  }

  searchPhrase(type: string, phrase: string) {
    const url = this.url + type + '/?search=' + phrase;
    
    return this.http.get(url);
  }

  searchObject(type: string, objectId: string) {
    const url = this.url + type + '/' + objectId + '/';

    return this.http.get(url);
  }

}
