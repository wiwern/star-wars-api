export class Film {
    title: string;
    episode_id: number;
    opening_crawl: string;
    characters: Array<string>;
    director: string;
    created: string;
    planets: Array<string>;
    producer: string;
    species: Array<string>;
    starships: Array<string>;
    url:string;
    vehicles: Array<string>;
    release_date: string;

    constructor(title: string,
        episode_id: number,
        opening_crawl: string,
        characters: Array<string>,
        director: string,
        created: string,
        planets: Array<string>,
        producer: string,
        species: Array<string>,
        starships: Array<string>,
        url:string,
        vehicles: Array<string>,
        release_date:string) {
            this.title = title;
            this.episode_id = episode_id;
            this.opening_crawl = opening_crawl;
            this.characters = characters;
            this.director = director;
            this.created = created;
            this.planets = planets;
            this.producer = producer;
            this.species = species;
            this.starships = starships;
            this.url = url;
            this.vehicles = vehicles;
            this.release_date = release_date;
        }
}
