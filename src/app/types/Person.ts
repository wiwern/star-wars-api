export class Person {
    birth_year: string;
    created: string;
    edited: string;
    eye_color: string;
    films: Array<string>;
    gender: string;
    hair_color: string;
    height: string;
    homeworld: string;
    mass: string;
    name: string;
    skin_color: string;
    species: Array<string>;
    starships: Array<string>;
    url: string;
    vehicles: Array<string>;

    constructor(birth_year: string,
        created: string,
        edited: string,
        eye_color: string,
        films: Array<string>,
        gender: string,
        hair_color: string,
        height: string,
        homeworld: string,
        mass: string,
        name: string,
        skin_color: string,
        species: Array<string>,
        starships: Array<string>,
        url: string,
        vehicles: Array<string>) {
            this.birth_year = birth_year;
            this.created = created;
            this.edited = edited;
            this.eye_color = eye_color;
            this.films = films;
            this.gender = gender;
            this.hair_color = hair_color;
            this.height = height;
            this.homeworld = homeworld;
            this.mass = mass;
            this.name = name;
            this.skin_color = skin_color;
            this.species = species;
            this.starships = starships;
            this.url = url;
            this.vehicles = vehicles;
    }
}