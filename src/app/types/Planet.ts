export class Planet {
    climate: string;
    created: string;
    diameter: string;
    edited: string;
    films: Array<string>;
    gravity: string;
    name: string;
    orbital_period: string;
    population: string;
    residents: Array<string>;
    rotation_period: string;
    surface_water: string;
    terrain: string;
    url: string;

    constructor(
        climate: string,
        created: string,
        diameter: string,
        edited: string,
        films: Array<string>,
        gravity: string,
        name: string,
        orbital_period: string,
        population: string,
        residents: Array<string>,
        rotation_period: string,
        surface_water: string,
        terrain: string,
        url: string,
    ) {
        this.climate = climate;
        this.created = created;
        this.diameter = diameter;
        this.edited = edited;
        this.films = films;
        this.gravity = gravity;
        this.name = name;
        this.orbital_period = orbital_period;
        this.population = population;
        this.residents = residents;
        this.rotation_period = rotation_period;
        this.surface_water = surface_water;
        this.terrain = terrain;
        this.url = url;
    }
}