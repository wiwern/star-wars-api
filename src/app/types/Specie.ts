export class Specie {
    average_height: string;
    average_lifespan: string;
    classification: string;
    created: string;
    designation: string;
    edited: string;
    eye_colors: string;
    films: Array<string>;
    hair_colors: string;
    homeworld: string;
    language: string;
    name: string;
    people: Array<string>;
    skin_colors: string;
    url: string;

constructor(average_height: string,
    average_lifespan: string,
    classification: string,
    created: string,
    designation: string,
    edited: string,
    eye_colors: string,
    films: Array<string>,
    hair_colors: string,
    homeworld: string,
    language: string,
    name: string,
    people: Array<string>,
    skin_colors: string,
    url: string) {
        this.average_height = average_height;
        this.average_lifespan = average_lifespan;
        this.classification = classification;
        this.created = created;
        this.designation = designation;
        this.edited = edited;
        this.eye_colors = eye_colors;
        this.films = films;
        this.hair_colors = hair_colors;
        this.homeworld = homeworld;
        this.language = language;
        this.name = name;
        this.people = people;
        this.skin_colors = skin_colors;
        this.url = url;

    }
}