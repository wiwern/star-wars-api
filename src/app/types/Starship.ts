export class Starship {
    MGLT: string
    cargo_capacity: string;
    consumables: string;
    cost_in_credits: string;
    created: string;
    crew: string;
    edited: string;
    films: Array<string>;
    hyperdrive_rating: string;
    length: string;
    manufacturer: string;
    max_atmosphering_speed: string;
    model: string;
    name: string;
    passengers: string;
    pilots: Array<string>;
    starship_class: string;
    url: string;

    constructor(MGLT: string,
        cargo_capacity: string,
        consumables: string,
        cost_in_credits: string,
        created: string,
        crew: string,
        edited: string,
        films: Array<string>,
        hyperdrive_rating: string,
        length: string,
        manufacturer: string,
        max_atmosphering_speed: string,
        model: string,
        name: string,
        passengers: string,
        pilots: Array<string>,
        starship_class: string,
        url: string) {
          this.MGLT = MGLT;
          this.cargo_capacity =cargo_capacity;
          this.consumables = consumables;
          this.cost_in_credits = cost_in_credits;
          this.created = created;
          this.crew = crew;
          this.edited = edited;
          this.films = films;
          this.hyperdrive_rating = hyperdrive_rating;
          this.length = length;
          this.manufacturer = manufacturer;
          this.max_atmosphering_speed = max_atmosphering_speed;
          this.model = model;
          this.name = name;
          this.passengers = passengers;
          this.pilots = pilots;
          this.starship_class = starship_class;
          this.url = url;
    }
}