"use strict";
exports.__esModule = true;
exports.Starship = void 0;
var Starship = /** @class */ (function () {
    function Starship(MGLT, cargo_capacity, consumables, cost_in_credits, created, crew, edited, films, hyperdrive_rating, length, manufacturer, max_atmosphering_speed, model, name, passengers, pilots, starship_class, url) {
        this.MGLT = MGLT;
        this.cargo_capacity = cargo_capacity;
        this.consumables = consumables;
        this.cost_in_credits = cost_in_credits;
        this.created = created;
        this.crew = crew;
        this.edited = edited;
        this.films = films;
        this.hyperdrive_rating = hyperdrive_rating;
        this.length = length;
        this.manufacturer = manufacturer;
        this.max_atmosphering_speed = max_atmosphering_speed;
        this.model = model;
        this.name = name;
        this.passengers = passengers;
        this.pilots = pilots;
        this.starship_class = starship_class;
        this.url = url;
    }
    return Starship;
}());
exports.Starship = Starship;
